# SLR
A better (or at least nicer looking) School Loop app for Android.

_*Active development paused for the time being (may resume in the future!)*_

_*Pre-release software.*_

## Documentation
...currently does not exist. Come back later!

## Installation
No pre-built apk files are currently available, so you'll need to manually build the apk and install it yourself.

Building is fairly simple with the included `gradlew` tool. Just run `gradlew assembleDebug` from the root directory of the project to create a debug build of the app.
Then install the apk as you normally would (enable installation from unknown sources, transfer the apk, etc.)

## Known Issues:
- [ ] Transition animations stutter because of simultaneous data-loading from the network
- [ ] Relaunching the app can cause crashes (no NetComponent) (can be fixed with `savedInstanceState`)
- [ ] Database storing is essentially useless since logins are impossible without a connection (will be fixed with login mechanism reimplementation)
- [ ] Incomplete & incohesive settings
- [ ] Complete lack of feature parity

## License
    Copyright 2016 David Cao

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.