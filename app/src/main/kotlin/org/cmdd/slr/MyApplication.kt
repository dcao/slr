package org.cmdd.slr

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import org.cmdd.slr.dagger.component.BaseComponent
import org.cmdd.slr.dagger.component.DaggerBaseComponent
import org.cmdd.slr.dagger.component.DaggerNetComponent
import org.cmdd.slr.dagger.component.NetComponent
import org.cmdd.slr.dagger.module.AppModule
import org.cmdd.slr.dagger.module.AuthModule
import org.cmdd.slr.dagger.module.NetModule
import org.cmdd.slr.data.SLInfo
import org.cmdd.slr.data.model.UserState
import timber.log.Timber

class MyApplication : Application() {
    lateinit var baseComponent: BaseComponent
    lateinit var netComponent: NetComponent

    lateinit var userState: UserState

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        baseComponent = DaggerBaseComponent.builder()
            .appModule(AppModule(this))
            .authModule(AuthModule(false))
            .build()

        initRealmConfiguration()
    }

    private fun initRealmConfiguration() {
        Realm.init(this)

        val mConfig = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(mConfig)
    }

    fun initNetState(slInfo: SLInfo): NetComponent {
        netComponent = DaggerNetComponent.builder()
                .netModule(NetModule(slInfo))
                .build()

        return netComponent
    }
}