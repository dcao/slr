package org.cmdd.slr.util

import android.util.Log
import timber.log.Timber

class ReleaseTree : Timber.Tree() {
    override fun isLoggable(priority: Int): Boolean {
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) return false else return true
    }

    // TODO: Implement
    override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
        if (isLoggable(priority)) {
            super.log(priority, tag, message, t)
        }
    }
}