package org.cmdd.slr.util

import android.os.Build

object ApiUtils {
    fun transitionsEnabled(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
    }
}