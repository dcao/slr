package org.cmdd.slr.data.auth

import android.content.SharedPreferences
import android.net.Uri
import org.cmdd.slr.data.SLInfo

// WARNING: This stores the key in SharedPreferences
// FIXME: Switch to SecureSLKeyManager
/**
 * A credentials storage system which relies on SharedPreferences to store user credentials.
 * Note that this system *is not secure* if the user has root access and can view/modify the SharedPreferences.
 *
 * @param prefs A SharedPreferences object to store/retrieve credentials from
 *
 * @see SecureSLKeyManager
 */
class SLKeyManager(val prefs: SharedPreferences) : AuthManager {

    private val privKeyAlias = "base64Test"

    private val userKey       = "slrUser"
    private val passKey       = "slrPass"
    private val schoolNameKey = "slrSchoolName"
    private val baseUrlKey    = "slrBaseUrl"

    override fun getInfo(): SLInfo {
        try {
            if (!infoStored()) {
                // TODO: Throw the error here
                setKeys()
            }

            val sk = getKeys()

            // Double bang to throw nullpointerexception, which will be handled
            val user = prefs.getString(userKey, null)!!
            val pass = prefs.getString(passKey, null)!!
            val name = prefs.getString(schoolNameKey, null)!!
            val url  = Uri.parse(prefs.getString(baseUrlKey, null))!!

            val cipherUser = AesCbcWithIntegrity.CipherTextIvMac(user)
            val cipherPass = AesCbcWithIntegrity.CipherTextIvMac(pass)

            val plainUser = AesCbcWithIntegrity.decryptString(cipherUser, sk)
            val plainPass = AesCbcWithIntegrity.decryptString(cipherPass, sk)

            return SLInfo(plainUser, plainPass, name, url)
        }
        catch (e: Exception) {
            // TODO: Log e
            // This will happen if getInfo is called before setInfo
            throw e
        }
    }

    override fun setInfo(mCredentials: SLInfo) {
        if (prefs.getString(privKeyAlias, null) == null) {
            setKeys()
        }
        val sk = getKeys()

        val (user, pass, schoolName, baseUrl) = mCredentials

        val encryptedUser = AesCbcWithIntegrity.encrypt(user, sk)
        val encryptedPass = AesCbcWithIntegrity.encrypt(pass, sk)

        prefs.edit()
            .putString(userKey, encryptedUser.toString())
            .putString(passKey, encryptedPass.toString())
            .putString(schoolNameKey, schoolName)
            .putString(baseUrlKey, baseUrl.toString())
            .apply()
    }

    override fun clearInfo() {
        prefs.edit()
            .remove(userKey)
            .remove(passKey)
            .remove(privKeyAlias)
            .apply()
    }

    override fun infoStored(): Boolean {
        val privateKeyStored = prefs.getString(privKeyAlias, null) != null
        val userStored = prefs.getString(userKey, null)         != null
        val passStored = prefs.getString(passKey, null)         != null
        return privateKeyStored && userStored && passStored
    }

    override fun setKeys() {
        val sk = AesCbcWithIntegrity.generateKey()
        prefs.edit().putString(privKeyAlias, sk.toString()).apply()
    }

    override fun getKeys(): AesCbcWithIntegrity.SecretKeys {
        val sepKeys = prefs.getString(privKeyAlias, null)
        return AesCbcWithIntegrity.keys(sepKeys)
    }
}