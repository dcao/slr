package org.cmdd.slr.data

import android.net.Uri

/**
 * A utility class to transport credentials and school information around within the app (or at least
 * between the SchoolSearchActivity and the MainActivity).
 * Rather than individually sending the user credentials and school information, they are bundled together
 * for convenience.
 *
 * @param username The username of the student
 * @param password The password of the student
 * @param schoolName The name of the school being logged into
 * @param baseUri The base Uri of the school being logged into (eg. your-school.schoolloop.com)
 */
data class SLInfo(val username: String, val password: String, val schoolName: String, val baseUri: Uri)
