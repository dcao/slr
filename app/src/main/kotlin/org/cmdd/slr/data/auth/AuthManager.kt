package org.cmdd.slr.data.auth

import org.cmdd.slr.data.SLInfo

/**
 * A generic interface for implementing different types of credential managers (classes which deal with
 * the storage and protection of user credentials)
 *
 * @see SLKeyManager
 */
interface AuthManager {
    fun getInfo(): SLInfo
    fun setInfo(mCredentials: SLInfo)
    fun infoStored(): Boolean
    fun setKeys()
    fun getKeys(): AesCbcWithIntegrity.SecretKeys
    fun clearInfo()
}