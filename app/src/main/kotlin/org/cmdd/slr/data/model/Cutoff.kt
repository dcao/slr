package org.cmdd.slr.data.model

import io.realm.RealmObject

/**
 * Defines a point where the letter grade of a student changes.
 * For example, a Cutoff with score 95 and grade C would mean that scores 95 and above (up to the
 * next highest Cutoff) are counted as a C.
 *
 * @property score The score of the cutoff
 * @property grade The grade of the cutoff
 */
open class Cutoff : RealmObject() {
    open var score: Double = 0.toDouble()
    open var grade: String = ""
}
