package org.cmdd.slr.data.remote

import org.cmdd.slr.data.model.Classroom
import org.cmdd.slr.data.model.CourseDetail
import org.cmdd.slr.data.model.UserState
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Works with 3.0.0; 11/15/2016 (no hashing)
 */
interface MobileApi {

    @GET("mapi/login?devOS=Android&hash=false&version=3.0.0&year=2016")
    fun login(): Call<UserState>

    @GET("mapi/report_card")
    fun report_card(@Query("studentID") studentID: Long): Call<List<Classroom>>

    @GET("mapi/progress_report")
    fun progress_report(@Query("studentID") studentID: Long, @Query("periodID") periodID: Long): Call<CourseDetail>
}
