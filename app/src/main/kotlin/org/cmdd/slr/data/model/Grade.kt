package org.cmdd.slr.data.model

import io.realm.RealmObject
import java.util.*

// FIXME: Deal with GradeDefinitions
/**
 * Stores information about the score earned on an Assignment.
 *
 * @property zero If the score should count as a zero.
 * @property grade The letter grade achieved on an Assignment
 * @property changedDate The most recent date on which this score or Assignment was modified.
 * @property excused If this score is to be excused, meaning its grade is irrelevant
 * @property courseRank [?]
 * @property courseAverage [?]
 * @property score The score achieved on an Assignment (essentially the number of points). Take this and divide it by the corresponding assignment's maxScore to get the percentScore.
 * @property comment Any associated comment with the score
 * @property assignment The associated Assignment metadata
 */
open class Grade : RealmObject() {
    open var zero: Boolean = false
    open var score: Double? = null
    open var scoreBase64: String? = null
    open var grade: String? = null
    open var changedDate: Date? = null
    open var excused: Boolean = false
    open var courseRank: Int = -1
    open var courseAverage: Double = -1.0.toDouble()
    open var comment: String = ""
    open var assignment: Assignment? = null
}