package org.cmdd.slr.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.parceler.Parcel
import org.parceler.ParcelPropertyConverter
import java.util.*

// http://stackoverflow.com/questions/27621840/usage-of-parceler-parcel-with-realm-io-android & http://parceler.org/#advanced_configuration
// TODO: JSR-310? Use another property with custom getter/setter which uses lastUpdated as its backing prop
// See https://kotlinlang.org/docs/reference/properties.html#backing-fields and http://stackoverflow.com/questions/19431234/converting-between-java-time-localdatetime-and-java-util-date
// You can even set lastUpdated as a long
// @Transient to ignore fields from parcel
/**
 * A course from the progress_report
 * This does not include all of the specific information about a course (ie. assignments). For that,
 * refer to [CourseDetail].
 *
 * @property courseName The title/name of the course
 * @property periodID The unique ID number given to each period of each course
 * @property period The period in which this course occurs
 * @property teacherID The unique ID number of a teacher
 * @property teacherName The name of the teacher
 * @property grade The prettified letter grade the student has earned in the course. (ie. A+, etc.)
 * @property score The score the student has earned in the course (ie. 95.22)
 * @property lastUpdated The last time the course had any updates; includes posting new assignments and grading existing ones
 * @property coTeacherID The unique ID number of the co-teacher, if there is one
 * @property coTeacherName The name of the co-teacher, if there is one
 *
 * @see CourseDetail
 */
@Parcel(value = Parcel.Serialization.BEAN, analyze = arrayOf(Classroom::class))
open class Classroom : RealmObject() {
    open var courseName: String = ""
    @PrimaryKey open var periodID: Long = 0
    open var period: Int = 0
    open var teacherID: Long = 0
    open var teacherName: String = ""
    open var grade: String? = null
    open var score: Double? = null
    @ParcelPropertyConverter(DateParcelConverter::class) open var lastUpdated: Date? = null
    open var coTeacherID: Long = 0
    open var coTeacherName: String = ""
}

