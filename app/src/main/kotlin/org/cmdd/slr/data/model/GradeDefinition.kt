package org.cmdd.slr.data.model

import io.realm.RealmObject

/**
 * A definition which defines a grade abbreviation.
 * For example, if a teacher uses M to mark a missing assignment worth 0 points, this class will
 * contain a key M and a value of 0
 *
 * @property keyBase64 The key, encoded in Base64
 * @property key The key
 * @property valueBase64 The corresponding value, encoded in Base64
 * @property value The corresponding value
 */
open class GradeDefinition : RealmObject() {
    open var keyBase64: String? = null
    open var key: String? = null
    open var valueBase64: String? = null
    open var value: String? = null
}
