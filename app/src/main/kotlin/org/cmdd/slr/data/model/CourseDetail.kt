package org.cmdd.slr.data.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

// TODO: Do we even need the grade defs prop?
/**
 * A class to represent the JSON object returned by a call to progress_report.
 * Contains all the details about a course: every graded assignment that has been published,
 * information about the teacher, definitions for grade abbreviations, etc.
 *
 * @property periodID The unique ID number given to each period of each course
 * @property lastUpdated The date of the last time the course was updated - not limited to new grades
 * @property score The numerical score the user has in this course
 * @property useWeighting Whether or not category weighting should be taken into account
 * @property grade The letter grade the user has
 * @property periodAverage [?]
 * @property precision The number of decimals the grade should be reported to
 * @property numberOfStudentsInCourse The number of students enrolled in the course
 * @property hasScore If this course has any grades input yet
 * @property dropped If this course is not being taken by a user any longer
 * @property courseRank [?]
 * @property courseAverage [?]
 * @property scoreChange The delta in score between the last update and the most recent update
 * @property grades The list of grades a user has
 * @property gradeDefinitions The definitive list of abbreviations (eg. E, M) used in scoring, and their properties
 * @property categories All the categories of a course
 * @property gradingScale The points at which a grade changes (eg. 93 is the minimum for an A)
 */
open class CourseDetail : RealmObject() {
    @PrimaryKey open var periodID: Long = 0
    open var lastUpdated: Date? = null
    open var score: Double = 0.toDouble()
    open var useWeighting: Boolean = false
    open var grade: String = ""
    open var periodAverage: String? = null
    open var precision: Int = 0
    open var numberOfStudentsInCourse: Int = 0
    open var hasScore: Boolean = false
    open var dropped: Boolean = false
    open var courseRank: Int = -1
    open var courseAverage: Double = -1.toDouble()
    open var scoreChange: Double = 0.toDouble()

    open var grades: RealmList<Grade>? = null
    open var gradeDefinitions: RealmList<GradeDefinition>? = null
    open var categories: RealmList<Category>? = null
    open var gradingScale: RealmList<Cutoff>? = null
}