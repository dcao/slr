package org.cmdd.slr.data.model

import io.realm.RealmObject

/**
 * The category an assignment is in.
 *
 * @property name The category's name
 * @property score The current score the user has in the category
 * @property weight The weight of the category when calculating a weighted course grade
 * @property hasScore Whether or not a score exists for this category
 * @property courseRank [?]
 * @property courseAverage [?]
 */
open class Category : RealmObject() {
    open var name: String = ""
    open var score: Double = 0.toDouble()
    open var weight: Double = 0.toDouble()
    open var hasScore: Boolean = false
    open var courseRank: Int = -1
    open var courseAverage: Double = -1.0.toDouble()
}