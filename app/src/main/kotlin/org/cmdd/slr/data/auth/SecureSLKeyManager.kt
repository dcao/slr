package org.cmdd.slr.data.auth

import android.content.Context
import timber.log.Timber
import java.security.KeyStore

// TODO: Store key with SharedPrefs if below 4.3 to maintain compatibility
// TODO: Do we have to do all the file stuff?
// TODO: Do we even need to use AndroidKeyStore?
// TODO: Logout somehow
class SecureSLKeyManager(ctx: Context) {

    val ctx = ctx
    val prefsName = "org.duhpster.slr"
    val ks = KeyStore.getInstance("AndroidKeyStore")

    val privConfKeyAlias = "slrPrivConfKey"
    val privInteKeyAlias = "slrPrivInteKey"

    val userKey = "slrUser"
    val passKey = "slrPass"

    // This is only being used since for some reason "Protection parameters must be specified when importing a symmetric key"
    val pwpt = KeyStore.PasswordProtection("Great password!".toCharArray())

    fun getCredentials(): Pair<String, String> {
        ks.load(null)
        try {
            val skConfEntry = ks.getEntry(privConfKeyAlias, null) as KeyStore.SecretKeyEntry
            val skInteEntry = ks.getEntry(privInteKeyAlias, null) as KeyStore.SecretKeyEntry

            val skConf = skConfEntry.secretKey
            val skInte = skInteEntry.secretKey

            var prefs = ctx.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
            // Double bang to throw nullpointerexception, which will be handled
            val user = prefs.getString(userKey, null)!!
            val pass = prefs.getString(passKey, null)!!

            val cipherUser = AesCbcWithIntegrity.CipherTextIvMac(user)
            val cipherPass = AesCbcWithIntegrity.CipherTextIvMac(pass)

            val sk = AesCbcWithIntegrity.SecretKeys(skConf, skInte)

            val plainUser = AesCbcWithIntegrity.decryptString(cipherUser, sk)
            val plainPass = AesCbcWithIntegrity.decryptString(cipherPass, sk)

            return Pair(plainUser, plainPass)
        }
        catch (e: Exception) {
            // TODO: Log e
            throw e
        }
    }

    // TODO: Implement
    fun setCredentials(user: String, pass: String) {
        ks.load(null)
        if (ks.getEntry(privConfKeyAlias, null) == null || ks.getEntry(privInteKeyAlias, null) == null) {
            Timber.d("NULL")
        }
        val skConf = (ks.getEntry(privConfKeyAlias, null) as KeyStore.SecretKeyEntry).secretKey
        val skInte = (ks.getEntry(privInteKeyAlias, null) as KeyStore.SecretKeyEntry).secretKey

        val sk = AesCbcWithIntegrity.SecretKeys(skConf, skInte)

        val encryptedUser = AesCbcWithIntegrity.encrypt(user, sk)
        val encryptedPass = AesCbcWithIntegrity.encrypt(pass, sk)

        val prefs = ctx.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
        prefs.edit()
            .putString(userKey, encryptedUser.toString())
            .putString(passKey, encryptedPass.toString())
            .apply()
    }

    fun setKeys() {
        val sk = AesCbcWithIntegrity.generateKey()
        val skConf = KeyStore.SecretKeyEntry(sk.confidentialityKey)
        val skInte = KeyStore.SecretKeyEntry(sk.integrityKey)
        ks.setEntry("slrPrivConfKey", skConf, null)
        ks.setEntry("slrPrivInteKey", skInte, null)
    }
}