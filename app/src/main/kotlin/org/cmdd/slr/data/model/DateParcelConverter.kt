package org.cmdd.slr.data.model

import org.parceler.ParcelConverter
import java.util.*

/**
 * Contains methods for converting bog standard java Dates into a Parcelable format by
 * getting their UTC timestamp (milliseconds since Jan 1, 1970)
 */
class DateParcelConverter : ParcelConverter<Date> {
    override fun fromParcel(parcel: android.os.Parcel): Date? {
        val time = parcel.readLong()
        if (time < 0) return null
        return Date(time)
    }

    override fun toParcel(input: Date?, parcel: android.os.Parcel) {
        if (input == null) {
            parcel.writeLong(-1)
        } else {
            parcel.writeLong(input.time)
        }
    }
}