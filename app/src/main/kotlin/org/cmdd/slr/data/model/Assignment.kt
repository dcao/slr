package org.cmdd.slr.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * The user-agnostic metadata of an assignment. This does not include any scores or when the grade was
 * posted, etc. This only includes information which would have been known about the assignment before
 * grades are posted, and are consistent between all students.
 *
 * @property dueDate The due date of the assignment
 * @property title The title of the assignment
 * @property maxPoints The maximum points possible on an assignment
 * @property systemID The internal unique ID number of the assignment
 * @property categoryName The name of the category the assignment is in
 */
open class Assignment : RealmObject() {
    open var dueDate: Date? = null
    open var title: String? = null
    open var maxPoints: Double = 0.toDouble()
    @PrimaryKey open var systemID: Long = 0
    open var categoryName: String? = null
}
