package org.cmdd.slr.data.model

import android.net.Uri

/**
 * Class which represents the returned values from a login call.
 * Also holds important state about the student and their login session (like the student's name,
 * ID number, the school's name, and the base Uri of the school.
 *
 * @property fullName The full name of the student, formatted as Last, First
 * @property email The email of the student
 * @property studentID The unique ID number of the student (this is used in all calls to the mobile API)
 * @property schoolName The name of the school being logged into
 * @property schoolUri The base Uri of the school being logged into (eg. your-school.schoolloop.com)
 */
data class UserState(val fullName: String, val email: String, val studentID: Long,
                     val schoolName: String, val schoolUri: Uri)