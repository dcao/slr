package org.cmdd.slr.ui

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import org.cmdd.slr.R
import org.jsoup.Jsoup
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*



// TODO: Clear button for search field?
class SchoolSearchActivity : AppCompatActivity() {

    val SEARCH_DELAY: Long = 1000
    val HELP_PAIR = Pair("Search for a school to get started.", "Search suggestions will appear down here!")

    var existing_name = ""

    // This was originally called testData because it was just that: test data.
    // Unfortunately, even though we're no longer using testData, the ArrayList being read from
    // is still named as such.
    val testData = arrayListOf(
            HELP_PAIR
    )
    val adapter = SchoolSearchAdapter(testData)

    @BindView(R.id.root_view) lateinit var rootView: LinearLayout
    @BindView(R.id.toolbar_ss) lateinit var toolbar: Toolbar
    @BindView(R.id.school_search) lateinit var schoolField: EditText
    @BindView(R.id.school_search_progress) lateinit var schoolProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)

            window.enterTransition = TransitionInflater.from(this).inflateTransition(R.transition.school_search_activity_enter)
            window.allowEnterTransitionOverlap = false
            window.allowReturnTransitionOverlap = false
        }

        setContentView(R.layout.activity_school_search)
        ButterKnife.bind(this)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.title = "School search"

        val rvSchools = findViewById(R.id.rv_schools) as RecyclerView
        rvSchools.setHasFixedSize(true)
        rvSchools.adapter = adapter
        rvSchools.layoutManager = LinearLayoutManager(this)

        if (!intent.getStringExtra("existing_name").isBlank()) {
            existing_name = intent.getStringExtra("existing_name")
            schoolField.setText(existing_name)
        }

        adapter.clicks.subscribe { p ->
            if (p.equals(HELP_PAIR)) {
                Timber.d("Initial help pair clicked!")
            } else {
                Timber.d("Item clicked! %s, %s", p.first, p.second)
                schoolField.setText(p.first)
                // TODO: Send pair instead?
                val intent = Intent()
                intent.putExtra("school_name", p.first)
                intent.putExtra("school_url", p.second)
                setResult(Activity.RESULT_OK, intent)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) finishAfterTransition() else finish()
            }
        }

        schoolField.addTextChangedListener(
                object : TextWatcher {
                    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    }

                    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                    }

                    private var timer = Timer()

                    override fun afterTextChanged(s: Editable) {
                        // Whenever someone enters text in the search field, the search results become
                        // irrelevant. As a result, we clear the data each time the text changes.
                        // The if exists to remove unnecessary clears when someone has just changed
                        // the text and the data is already empty.
                        if (!testData.isEmpty()) {
                            adapter.clearData()
                        }

                        // Get the current text from the schoolField.
                        var str = s.toString()

                        // If the field is blank, show the default help text and cancel any scheduled
                        // events (to prevent a network call)
                        if (str.isBlank()) {
                            schoolProgressBar.visibility = View.GONE
                            testData.add(HELP_PAIR)
                            adapter.notifyItemInserted(0)
                            timer.cancel()
                        } else {
                            schoolProgressBar.visibility = View.VISIBLE
                            timer.cancel()
                            timer = Timer()
                            timer.schedule(
                                    object : TimerTask() {
                                        override fun run() {
                                            // TODO: Handle errors (no internet etc)
                                            // TODO: Use Handler?
                                            try {
                                                if (!str.isBlank()) {
                                                    Timber.d("Starting networking ops with string $str")

                                                    var slist = ArrayList<Pair<String, String>>()

                                                    val client = OkHttpClient()
                                                    val formBody = FormBody.Builder()
                                                            .add("search", str)
                                                            .add("no_default_event", "t")
                                                            .build()

                                                    val request = Request.Builder()
                                                            .url("http://search.schoolloop.com/misc/school_search")
                                                            .post(formBody)
                                                            .build()

                                                    val response = client.newCall(request).execute()
                                                    if (!response.isSuccessful) {
                                                        setResult(Activity.RESULT_CANCELED)
                                                        finish()
                                                    }

                                                    val html = Jsoup.parse(response.body().string())
                                                    val schools = html.select("table").first().select("td").select("a")

                                                    adapter.clearData()

                                                    for (item in schools) {
                                                        slist.add(Pair(item.text(), item.attr("href")))
                                                    }

                                                    if (slist.isEmpty()) {
                                                        Snackbar.make(rootView,
                                                                "No results found! Try a different search.",
                                                                Snackbar.LENGTH_SHORT).show()
                                                    }

                                                    testData.addAll(slist)
                                                    adapter.notifyItemRangeInserted(0, slist.size)
                                                    runOnUiThread {
                                                        schoolProgressBar.visibility = View.GONE
                                                    }
                                                }
                                            } catch (e: UnknownHostException) {
                                                // If there's no internet connection, okhttp will throw
                                                // this exception.
                                                // TODO: Maybe handle better (at beginning?)
                                                // TODO: Maybe send back to MainActivity?
                                                Snackbar.make(rootView,
                                                        "No internet connection found! Please try searching later.",
                                                        Snackbar.LENGTH_LONG).show()
                                                runOnUiThread {
                                                    schoolProgressBar.visibility = View.GONE
                                                }
                                            } catch (e: ConnectException) {
                                                // If there's no internet connection, okhttp will throw
                                                // this exception.
                                                // TODO: Maybe handle better (at beginning?)
                                                // TODO: Maybe send back to MainActivity?
                                                Snackbar.make(rootView,
                                                        "No internet connection found! Please try searching later.",
                                                        Snackbar.LENGTH_LONG).show()
                                                runOnUiThread {
                                                    schoolProgressBar.visibility = View.GONE
                                                }
                                            } catch (e: SocketTimeoutException) {
                                                Snackbar.make(rootView,
                                                        "School search is currently unavailable! Please try searching later.",
                                                        Snackbar.LENGTH_LONG).show()
                                                runOnUiThread {
                                                    schoolProgressBar.visibility = View.GONE
                                                }
                                            }
                                        }
                                    },
                                    SEARCH_DELAY)
                        }
                    }
                })

        // Move schoolField's cursor to the end
        schoolField.setSelection(schoolField.text.length)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            cancelSearch()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        cancelSearch()
        super.onBackPressed()
    }

    // Helper function to run when the search is cancelled, whether it's because the back button or
    // the back nav button was pushed
    fun cancelSearch() {
        setResult(Activity.RESULT_CANCELED)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) finishAfterTransition() else finish()
    }
}
