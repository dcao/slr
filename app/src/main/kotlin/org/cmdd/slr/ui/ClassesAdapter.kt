package org.cmdd.slr.ui

import android.graphics.Color
import android.os.Build
import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.cmdd.slr.R
import org.cmdd.slr.data.model.Classroom
import rx.lang.kotlin.PublishSubject

// TODO: Implement onClick functionality
class ClassesAdapter : RecyclerView.Adapter<ClassesAdapter.ViewHolder>() {

    val mData: SortedList<Classroom> = SortedList(Classroom::class.java, object : SortedList.Callback<Classroom>() {
        override fun compare(o1: Classroom, o2: Classroom): Int {
            if (o1 == o2) return 0
            return o1.period.compareTo(o2.period)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
        }

        override fun areContentsTheSame(oldItem: Classroom, newItem: Classroom): Boolean {
            // return whether the items' visual representations are the same or not.
            val names    = oldItem.courseName == newItem.courseName
            val teachers = oldItem.teacherName == newItem.teacherName
            val scores   = oldItem.score == newItem.score

            return names && teachers && scores
        }

        override fun areItemsTheSame(item1: Classroom, item2: Classroom): Boolean {
            return item1.periodID == item2.periodID
        }
    })

    private val onClickSubject = PublishSubject<Classroom>()
    val clicks = onClickSubject.asObservable()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mClassName: TextView
        var mClassPeriod: TextView
        var mClassTeacher: TextView
        var mClassGrade: TextView
        var mClassScore: TextView
        init {
            mClassName = itemView.findViewById(R.id.class_item_name) as TextView
            mClassPeriod = itemView.findViewById(R.id.class_item_period) as TextView
            mClassTeacher = itemView.findViewById(R.id.class_item_teacher) as TextView
            mClassGrade = itemView.findViewById(R.id.class_item_grade) as TextView
            mClassScore = itemView.findViewById(R.id.class_item_score) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val schoolView = inflater.inflate(R.layout.item_class, parent, false)

        val viewHolder = ViewHolder(schoolView)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val classroom = mData[position]
        val classNameView = holder.mClassName
        val classPeriodView = holder.mClassPeriod
        val classTeacherView = holder.mClassTeacher
        val classGradeView = holder.mClassGrade
        val classScoreView = holder.mClassScore

        // TODO: String resources
        classNameView.text = classroom.courseName
        classPeriodView.text = "Period ${classroom.period}"
        classTeacherView.text = classroom.teacherName
        classGradeView.text = classroom.grade ?: ""

        if (classroom.score != null) {
            classScoreView.text = "${classroom.score}%"
            classScoreView.setTextColor(color(classroom.score!!))
        } else {
            classScoreView.text = "No score"
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) holder.itemView.transitionName = "course_$position"

        holder.itemView?.setOnClickListener {
            onClickSubject.onNext(classroom)
        }
    }

    override fun getItemCount(): Int {
        return mData.size()
    }

    fun clearData() {
        mData.beginBatchedUpdates()
        while (mData.size() > 0) {
            mData.removeItemAt(mData.size() - 1)
        }
        mData.endBatchedUpdates()
    }

    fun color(score: Double): Int {
        // 50 is used as the min since anything 50 or below should be red
        val (MAX, MIN) = Pair(100.toDouble(), 50.toDouble())
        val sc = if (score > MAX) MAX else if (score < MIN) MIN else score
        val h = (((sc - MIN) * 120) / (MAX - MIN)).toFloat()
        // If you want white in the middle instead of yellow, scale this using abs(val - 50) / 50
        val s = 1f
        val v = 0.75f
        return Color.HSVToColor(floatArrayOf(h, s, v))
    }
}