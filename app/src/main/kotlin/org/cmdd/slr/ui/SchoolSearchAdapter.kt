package org.cmdd.slr.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.cmdd.slr.R
import rx.lang.kotlin.PublishSubject
import java.util.*

class SchoolSearchAdapter(dataset: ArrayList<Pair<String, String>>) : RecyclerView.Adapter<SchoolSearchAdapter.ViewHolder>() {
    var mDataSet = dataset

    private val onClickSubject = PublishSubject<Pair<String, String>>()
    val clicks = onClickSubject.asObservable()

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mSiteName = itemView.findViewById(R.id.site_name) as TextView
        val mSiteUrl  = itemView.findViewById(R.id.site_url) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val context = parent?.context
        val inflater = LayoutInflater.from(context)

        val schoolView = inflater.inflate(R.layout.item_school, parent, false)

        val viewHolder = ViewHolder(schoolView)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val school = mDataSet[position]

        val schoolName = holder?.mSiteName
        val schoolUrl =  holder?.mSiteUrl

        schoolName?.text = school.first
        schoolUrl?.text  = school.second

        holder?.itemView?.setOnClickListener { v ->
            onClickSubject.onNext(school)
        }
    }

    override fun getItemCount(): Int {
        return mDataSet.size
    }

    fun clearData() {
        val size = mDataSet.size
        if (size > 0) {
            for (i in 0..size - 1) {
                mDataSet.removeAt(0)
            }

            this.notifyItemRangeRemoved(0, size)
        }
    }
}
