package org.cmdd.slr.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import butterknife.BindView
import butterknife.ButterKnife
import io.realm.RealmList
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator
import org.cmdd.slr.R
import org.cmdd.slr.data.model.Grade

class GradesFragment : Fragment() {

    @BindView(R.id.rv_assignments) lateinit var rvClasses: RecyclerView

    // TODO: Just init the adapter here? - this would avoid all the stuff with callbacks
    lateinit var mAdapter: GradesAdapter
    lateinit var mListener: CompleteListener

    val ANIM_DURATION: Long = 200

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_assignments, container, false)
        ButterKnife.bind(this, rootView)

        mAdapter = GradesAdapter()
        rvClasses.layoutManager = LinearLayoutManager(activity)

        rvClasses.adapter = mAdapter
        val animator = SlideInRightAnimator(AccelerateDecelerateInterpolator())
        animator.addDuration = ANIM_DURATION
        animator.changeDuration = ANIM_DURATION
        animator.removeDuration = ANIM_DURATION
        rvClasses.itemAnimator = animator

        mListener.onGradesComplete()

        return rootView
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mListener = activity as CompleteListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement OnArticleSelectedListener")
        }
    }

    fun load(data: RealmList<Grade>) {
        mAdapter.mData.addAll(data)
    }

    interface CompleteListener {
        fun onGradesComplete()
    }
}
