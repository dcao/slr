package org.cmdd.slr.ui

import android.os.Bundle
import android.preference.PreferenceFragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import de.psdev.licensesdialog.LicensesDialog
import org.cmdd.slr.BuildConfig
import org.cmdd.slr.R

class SettingsActivity : AppCompatActivity() {

    @BindView(R.id.toolbar_settings) lateinit var mToolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        ButterKnife.bind(this)
        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        fragmentManager.beginTransaction().replace(R.id.content_frame, AboutFragment()).commit()
    }

    class AboutFragment : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.prefs_root)
            val aboutPref = findPreference("about")
            aboutPref.title = "About SLR v${BuildConfig.VERSION_NAME}"

            val licensePref = findPreference("licenses")
            licensePref.setOnPreferenceClickListener {
                LicensesDialog.Builder(activity)
                    .setNotices(R.raw.notices)
                    .setIncludeOwnLicense(true)
                    .build()
                    .show()
                true
            }
        }
    }
}