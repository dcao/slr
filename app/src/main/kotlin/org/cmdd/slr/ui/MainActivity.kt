package org.cmdd.slr.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.text.InputType
import android.transition.TransitionInflater
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import org.cmdd.slr.MyApplication
import org.cmdd.slr.R
import org.cmdd.slr.dagger.component.BaseComponent
import org.cmdd.slr.data.SLInfo
import org.cmdd.slr.data.auth.AuthManager
import org.cmdd.slr.data.model.UserState
import org.cmdd.slr.data.remote.MobileApi
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

// TODO: Refactor and reorganize (move) everything
// TODO: Extract all the strings!
// TODO: Check if logged in already (credentials stored)
// TODO: Deal with teachers
// TODO: Use Gitlab/Github/Trello issues instead
// TODO: Send StudentParse object over intent
class MainActivity : AppCompatActivity() {

    var schoolName: String? = null
    var schoolUrl: String? = null

    val SEARCH_REQUEST = 2

    val MAX_ATTEMPTS = 4

    lateinit var mRootView: LinearLayout
    lateinit var schoolField: EditText
    lateinit var usernameField: EditText
    lateinit var passwordField: EditText

    @Inject lateinit var mAuth: AuthManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)

            window.exitTransition = TransitionInflater.from(this@MainActivity).inflateTransition(R.transition.main_activity_exit)
            window.allowEnterTransitionOverlap = false
            window.allowReturnTransitionOverlap = false
        }

        scrollView {
            mRootView = verticalLayout {
                id = R.id.root_view
                horizontalPadding = dip(24)
                topPadding = dip(56)
                descendantFocusability = LinearLayout.FOCUS_BEFORE_DESCENDANTS
                isFocusableInTouchMode = true

                textView {
                    textResource = R.string.main_header_title
                    textSize = 32f
                }.lparams(width = wrapContent, height = dip(72)) {
                    bottomMargin = dip(16)
                    gravity = Gravity.CENTER_HORIZONTAL
                }

                textView {
                    textResource = R.string.main_header_desc
                    textSize = 16f
                }.lparams(width = wrapContent, height = dip(72)) {
                    gravity = Gravity.CENTER
                }

                schoolField = editText(theme = R.style.AppTheme_NoActionBar_Splash_EditText) {
                    hintResource = R.string.main_school_search_label

                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
                    textColor = 0xffffff.opaque
                    isClickable = false
                    isCursorVisible = false
                    isFocusable = false
                    isFocusableInTouchMode = false
                    transitionName = "school_field"

                    onClick {
                        schoolFieldClick()
                    }
                }.lparams(width = matchParent, height = wrapContent) {
                    gravity = Gravity.CENTER
                    bottomMargin = dip(8)
                }

                usernameField = editText(theme = R.style.AppTheme_NoActionBar_Splash_EditText) {
                    hint = "Username"

                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
                    maxLines = 1
                    imeOptions = EditorInfo.IME_ACTION_NEXT
                    textColor = 0xffffff.opaque
                }.lparams(width = matchParent, height = wrapContent)

                passwordField = editText(theme = R.style.AppTheme_NoActionBar_Splash_EditText) {
                    hint = "Password"

                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                    maxLines = 1
                    imeOptions = EditorInfo.IME_ACTION_DONE
                    textColor = 0xffffff.opaque
                }.lparams(width = matchParent, height = wrapContent)

                appCompatButton {
                    textResource = R.string.main_btn_login_label

                    onClick {
                        Timber.d(this@MainActivity.getString(R.string.log_main_btn_login_click), schoolName, schoolUrl)

                        if (schoolUrl.isNullOrEmpty()) {
                            Snackbar.make(mRootView, this@MainActivity.getString(R.string.error_main_field_school_empty), Snackbar.LENGTH_SHORT).show()
                        }

                        if (usernameField.text.isNullOrEmpty()) {
                            usernameField.error = this@MainActivity.getString(R.string.error_main_field_user_empty)
                        }

                        if (passwordField.text.isNullOrEmpty()) {
                            passwordField.error = this@MainActivity.getString(R.string.error_main_field_pass_empty)
                        }

                        if (!schoolUrl.isNullOrEmpty() && !usernameField.text.isNullOrEmpty() && !passwordField.text.isNullOrEmpty() && !schoolName.isNullOrEmpty()) {
                            val credentials = SLInfo(usernameField.text.toString(), passwordField.text.toString(), schoolName ?: "<School not found>", Uri.parse(schoolUrl))
                            login(credentials)
                        }
                    }
                }.lparams(width = matchParent, height = wrapContent) {
                    gravity = Gravity.CENTER
                    topMargin = dip(16)
                }

            }.lparams(width = matchParent, height = matchParent)
        }.layoutParams = FrameLayout.LayoutParams(matchParent, matchParent)

        val component: BaseComponent = (application as MyApplication).baseComponent
        component.inject(this)

        if (mAuth.infoStored()) {
            val slInfo = mAuth.getInfo()
            schoolName = slInfo.schoolName
            schoolUrl = slInfo.baseUri.toString()

            schoolField.setText(schoolName)
            usernameField.setText(slInfo.username)
            passwordField.setText(slInfo.password)

            login(slInfo)
        }
    }

    inline fun ViewManager.appCompatButton(theme: Int = 0) = appCompatButton(theme) {}
    inline fun ViewManager.appCompatButton(theme: Int = 0, init: AppCompatButton.() -> Unit) = ankoView(::AppCompatButton, theme, init)

    fun schoolFieldClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (this.findViewById(android.R.id.navigationBarBackground) != null && this.findViewById(android.R.id.statusBarBackground) != null) {
                val mOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this,
                        android.support.v4.util.Pair(schoolField, "school_field"),
                        android.support.v4.util.Pair(this.findViewById(android.R.id.navigationBarBackground),
                                Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME),
                        android.support.v4.util.Pair(this.findViewById(android.R.id.statusBarBackground),
                                Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME))

                val intent = Intent(this, SchoolSearchActivity::class.java)

                intent.putExtra(this.getString(R.string.all_intent_key_existing_school_name), schoolField.text.toString())

                startActivityForResult(intent, SEARCH_REQUEST, mOptions.toBundle())

            } else {
                val mOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this,
                        android.support.v4.util.Pair(schoolField, "school_field"))

                val intent = Intent(this, SchoolSearchActivity::class.java)

                intent.putExtra(this.getString(R.string.all_intent_key_existing_school_name), schoolField.text.toString())
                startActivityForResult(intent, SEARCH_REQUEST, mOptions.toBundle())
            }
        } else {
            val mIntent = Intent(this, SchoolSearchActivity::class.java)
            mIntent.putExtra(this.getString(R.string.all_intent_key_existing_school_name), schoolField.text.toString())
            startActivityForResult(intent, SEARCH_REQUEST)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // val intent = Intent(this, SettingsActivity::class.java)
            // startActivity(intent)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    // TODO: Move property assignment here too?
    override fun onActivityReenter(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val mSchoolName = data?.getStringExtra(this.getString(R.string.all_intent_key_school_name))
            if (!mSchoolName.isNullOrBlank()) {
                schoolField.setText(mSchoolName, TextView.BufferType.NORMAL)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            SEARCH_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        schoolName = data.getStringExtra(this.getString(R.string.all_intent_key_school_name))
                        schoolUrl = data.getStringExtra(this.getString(R.string.all_intent_key_school_url))
                    } else {
                        // TODO: Implement
                    }
                }
            }
        }
    }

    // TODO: Auto-retry
    fun login(slInfo: SLInfo) {
        val mApp = (application as MyApplication)
        val netComponent = mApp.initNetState(slInfo)

        // Note that there are no intent extras anywhere!

        val progressDialog = ProgressDialog(this, R.style.AppCompatAlertDialogStyle)
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.isIndeterminate = true
        progressDialog.setTitle(R.string.main_progress_title)
        progressDialog.setMessage(this.getString(R.string.main_progress_description))
        progressDialog.setCancelable(false)
        val call = netComponent.retrofit().create(MobileApi::class.java).login()

        progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, this.getString(R.string.main_label_cancel), { dialog, which ->
            call.cancel()
            progressDialog.cancel()
        })
        progressDialog.show()

        call.enqueue(object : retrofit2.Callback<UserState> {
            override fun onResponse(call: Call<UserState>, response: Response<UserState>) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    mAuth.setInfo(slInfo)
                    Timber.i(this@MainActivity.getString(R.string.log_main_login_success))

                    mApp.userState = response.body()

                    val mIntent = Intent(this@MainActivity, HomeActivity::class.java)
                    startActivity(mIntent)
                    finish()
                } else {
                    Timber.d("Error: ${response.errorBody().string()}")
                    // FIXME: Finish
                }
            }

            override fun onFailure(call: Call<UserState>, t: Throwable) {
                progressDialog.dismiss()
                Timber.e(t)
                Snackbar.make(mRootView,
                        "No internet connection found! Please try searching later.",
                        Snackbar.LENGTH_LONG).show()
            }
        })

    }
}
