package org.cmdd.slr.ui

import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.cmdd.slr.R
import org.cmdd.slr.data.model.Grade
import java.text.DateFormat

class GradesAdapter() : RecyclerView.Adapter<GradesAdapter.ViewHolder>() {

    val mData = SortedList(Grade::class.java, object : SortedList.Callback<Grade>() {
        override fun compare(o1: Grade, o2: Grade): Int {
            if (o1 == o2) return 0
            return o1.assignment!!.dueDate!!.compareTo(o2.assignment!!.dueDate)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
        }

        override fun areContentsTheSame(oldItem: Grade, newItem: Grade): Boolean {
            // return whether the items' visual representations are the same or not.
            val names  = oldItem.assignment!!.title == newItem.assignment!!.title
            val dates  = oldItem.assignment!!.dueDate == newItem.assignment!!.dueDate
            val scores = oldItem.score == newItem.score

            return names && dates && scores
        }

        override fun areItemsTheSame(item1: Grade, item2: Grade): Boolean {
            return item1.assignment!!.systemID == item2.assignment!!.systemID
        }
    })

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mName     = itemView.findViewById(R.id.item_grade_name)     as TextView
        val mCategory = itemView.findViewById(R.id.item_grade_category) as TextView
        val mScore    = itemView.findViewById(R.id.item_grade_score)    as TextView
        val mDue      = itemView.findViewById(R.id.item_grade_due)      as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val mRootView = inflater.inflate(R.layout.item_assignment, parent, false)

        val mViewHolder = ViewHolder(mRootView)
        return mViewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mGrade = mData[position]

        holder.mName.text     = mGrade.assignment!!.title
        holder.mCategory.text = mGrade.assignment!!.categoryName
        holder.mScore.text    = if (mGrade.score != null) "${mGrade.score} / ${mGrade.assignment!!.maxPoints}" else "No score"
        // TODO: Respect date format settings
        // TODO: Inject?
        holder.mDue.text      = if (mGrade.assignment!!.dueDate != null) DateFormat.getDateInstance().format(mGrade.assignment!!.dueDate) else "No due date"
    }

    override fun getItemCount(): Int {
        return mData.size()
    }

    fun clearData() {
        mData.beginBatchedUpdates()
        while (mData.size() > 0) {
            mData.removeItemAt(mData.size() - 1)
        }
        mData.endBatchedUpdates()
    }
}