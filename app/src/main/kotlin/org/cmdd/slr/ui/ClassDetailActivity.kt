package org.cmdd.slr.ui

import android.os.Build
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.transition.TransitionInflater
import android.view.MenuItem
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.Window
import android.widget.ImageButton
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import io.realm.Realm
import org.cmdd.slr.MyApplication
import org.cmdd.slr.R
import org.cmdd.slr.data.model.Classroom
import org.cmdd.slr.data.model.CourseDetail
import org.cmdd.slr.data.remote.MobileApi
import org.cmdd.slr.util.ApiUtils
import org.parceler.Parcels
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber

class ClassDetailActivity : AppCompatActivity(), GradesFragment.CompleteListener {

    @BindView(R.id.toolbar_class_detail) lateinit var mToolbar: Toolbar
    @BindView(R.id.app_bar_class_detail) lateinit var mAppBar: AppBarLayout
    @BindView(R.id.class_detail_pager) lateinit var mPager: ViewPager
    @BindView(R.id.toolbar_class_title) lateinit var mToolbarText: TextView
    @BindView(R.id.toolbar_class_tabs) lateinit var mTabLayout: TabLayout
    @BindView(R.id.activity_class_detail_swipe_refresh) lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    lateinit var mBaseUrl: String
    lateinit var mClassroom: Classroom

    lateinit var realm: Realm

    val MAX_ATTEMPTS = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)

            window.enterTransition = TransitionInflater.from(this).inflateTransition(R.transition.class_detail_activity_exit)
        }

        setContentView(R.layout.activity_class_detail)
        ButterKnife.bind(this)

        val mApp = application as MyApplication

        mBaseUrl = intent.getStringExtra("base_url_string")

        // Postpone the transition until the window's decor view has
        // finished its layout.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition()

            val decor = window.decorView
            decor.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    decor.viewTreeObserver.removeOnPreDrawListener(this)
                    startPostponedEnterTransition()
                    return true
                }
            })
        }

        setSupportActionBar(mToolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        mClassroom = Parcels.unwrap(intent.getParcelableExtra(this.getString(R.string.all_intent_key_classroom)))
        mToolbarText.text = mClassroom.courseName

        setupTabLayout(mTabLayout, mPager)

        mSwipeRefreshLayout.setOnRefreshListener { net(mApp) }
        net(mApp)
        realm = Realm.getDefaultInstance()

        mPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {
                mSwipeRefreshLayout.isEnabled = state == ViewPager.SCROLL_STATE_IDLE
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.removeAllChangeListeners()
        realm.close()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                animateToolbar(false)
                finishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (ApiUtils.transitionsEnabled()) animateToolbar(false)
        super.onBackPressed()
    }

    // TODO: Cancel call if backed out beforehand
    // TODO: Two possible structures
    // a) Adding changeListener which loads data if not empty and then loads data if not empty in initData, then just change realm stuff here
    // b) Loading the data if it's not empty in initData, but not setting a changeListener, then manually updating everything here.
    //    Note that with this solution, if loading finishes before the fragment's onCreateView is
    //    finished, you will crash since the adapter hasn't been created yet.
    fun net(mApp: MyApplication) {
        mSwipeRefreshLayout.isRefreshing = true
        val rootView = (this.findViewById(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
        val netComponent = mApp.netComponent
        // TODO: Move this outta here
        val call = netComponent.retrofit().create(MobileApi::class.java).progress_report(mApp.userState.studentID, mClassroom.periodID)
        call.enqueue(object : retrofit2.Callback<CourseDetail> {
            override fun onResponse(call: Call<CourseDetail>, response: Response<CourseDetail>) {
                mSwipeRefreshLayout.isRefreshing = false
                if (response.isSuccessful) {
                    // Normally we would use executeTransactionAsync to silence the REALM_JAVA async stuff,
                    // but that breaks the app since we're accessing the RealmObject on a different thread
                    val body = response.body()
                    realm.executeTransaction {
                        realm.copyToRealmOrUpdate(body)
                    }
                    val gradesFrag = (mPager.adapter as ClassDetailViewPagerAdapter).getItem(0) as GradesFragment
                    gradesFrag.load(body.grades!!)
                } else {
                    // FIXME: Implement
                }
            }

            override fun onFailure(call: Call<CourseDetail>, t: Throwable) {
                mSwipeRefreshLayout.isRefreshing = false
                Timber.e(t)
                Snackbar.make(rootView,
                        "No internet connection found! Please try again later.",
                        Snackbar.LENGTH_LONG).show()
            }
        })

    }

    override fun onGradesComplete() {
        initData()
        // enter transitions aren't working for the title, so we do it manually!
        if (ApiUtils.transitionsEnabled()) animateToolbar(true)
    }

    fun initData() {
        val course = realm.where(CourseDetail::class.java).equalTo("periodID", mClassroom.periodID).findAll()
        if (course.isNotEmpty()) {
            val gradesFrag = (mPager.adapter as ClassDetailViewPagerAdapter).getItem(0) as GradesFragment
            gradesFrag.load(course.first().grades!!)
        } else {
            Timber.d("It's empty!")
        }
    }

    fun setupTabLayout(mtl: TabLayout, mPager: ViewPager) {
        val mAdapter = ClassDetailViewPagerAdapter(supportFragmentManager)
        mPager.adapter = mAdapter

        val mGradesFrag = GradesFragment()
        mAdapter.addFrag(mGradesFrag, this.getString(R.string.class_detail_tab_assignments_title))
        mtl.setupWithViewPager(mPager)
    }

    fun animateToolbar(enter: Boolean) {
        if (enter) {
            val baseDelay   = 50
            val textMove    = 200
            val textDelay   = baseDelay + 150
            val buttonMove  = 200
            val buttonDelay = baseDelay + 350

            mToolbarText.translationY = textMove.toFloat()

            mToolbarText.animate()
                    .translationYBy(textMove * -1f)
                    .setStartDelay(textDelay.toLong())
                    .start()

            for (i in 0..mToolbar.childCount - 1) {
                val mChild = mToolbar.getChildAt(i)

                if (mChild is ImageButton) {
                    mChild.translationX = buttonMove * -1f
                    mChild.animate()
                            .translationXBy(buttonMove.toFloat())
                            .setStartDelay(buttonDelay.toLong())
                            .start()
                }
            }
        } else {
            val baseDelay    = 0
            val textMove     = 200
            val textDelay    = baseDelay + 50
            val buttonMove   = 200
            val buttonDelay  = baseDelay + 85
            val animDuration = 125.toLong()

            mToolbarText.clearAnimation()

            mToolbarText.animate()
                    .translationYBy(textMove * -1f)
                    .setStartDelay(textDelay.toLong())
                    .setDuration(animDuration)
                    .start()

            for (i in 0..mToolbar.childCount - 1) {
                val mChild = mToolbar.getChildAt(i)

                if (mChild is ImageButton) {
                    mChild.clearAnimation()
                    mChild.animate()
                            .translationXBy(buttonMove * -1f)
                            .setStartDelay(buttonDelay.toLong())
                            .setDuration(animDuration)
                            .start()
                }
            }
        }
    }
}
