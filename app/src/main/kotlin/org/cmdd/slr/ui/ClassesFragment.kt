package org.cmdd.slr.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import butterknife.BindView
import butterknife.ButterKnife
import io.realm.Realm
import org.cmdd.slr.MyApplication
import org.cmdd.slr.R
import org.cmdd.slr.data.model.Classroom
import org.cmdd.slr.data.remote.MobileApi
import org.parceler.Parcels
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ClassesFragment : Fragment() {

    val MAX_ATTEMPTS = 3

    @BindView(R.id.rv_classes) lateinit var rvClasses: RecyclerView
    @BindView(R.id.fragment_classes_swipe_refresh) lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    lateinit var mAdapter: ClassesAdapter
    // TODO: Injection!
    lateinit var realm: Realm

    // This isn't straight injected since for some reason Dagger throws an error if you try
    lateinit var retrofit: Retrofit

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_classes, container, false)
        ButterKnife.bind(this, rootView)

        val netComponent = (activity.application as MyApplication).netComponent
        retrofit = netComponent.retrofit()

        realm = Realm.getDefaultInstance()

        mAdapter = ClassesAdapter()
        rvClasses.layoutManager = LinearLayoutManager(activity)
        rvClasses.adapter = mAdapter
        rvClasses.itemAnimator = DefaultItemAnimator()

        // Workaround to get mSwipeRefreshLayout to refresh properly on startup
        mSwipeRefreshLayout.post { mSwipeRefreshLayout.isRefreshing = true }
        mSwipeRefreshLayout.setOnRefreshListener { net(rootView) }

        mAdapter.clicks.subscribe {
            val mIntent = Intent(activity, ClassDetailActivity::class.java)
            mIntent.putExtra(resources.getString(R.string.all_intent_key_classroom), Parcels.wrap(Classroom::class.java, it))
            mIntent.putExtra("base_url_string", arguments.getString("base_url_string"))

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // For some reason the navigation bar isn't not transitioning so I have to do this
                if (activity.findViewById(android.R.id.navigationBarBackground) != null &&
                        activity.findViewById(android.R.id.statusBarBackground) != null) {
                    val mOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            activity,
                            android.support.v4.util.Pair(activity.findViewById(R.id.app_bar_home),
                                    resources.getString(R.string.all_xname_toolbar_home)),
                            android.support.v4.util.Pair(activity.findViewById(android.R.id.navigationBarBackground),
                                    Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME),
                            android.support.v4.util.Pair(activity.findViewById(android.R.id.statusBarBackground),
                                    Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME))
                    startActivity(mIntent, mOptions.toBundle())
                } else {
                    val mOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            activity,
                            android.support.v4.util.Pair(activity.findViewById(R.id.app_bar_home),
                                    resources.getString(R.string.all_xname_toolbar_home)))
                    startActivity(mIntent, mOptions.toBundle())
                }
            }
        }

        initData()
        net(rootView)

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.removeAllChangeListeners()
        realm.close()
    }

    // TODO: Cancel call if backed out beforehand
    // TODO: Two possible structures
    // a) Adding changeListener which loads data if not empty and then loads data if not empty in initData, then just change realm stuff here
    // b) Loading the data if it's not empty in initData, but not setting a changeListener, then manually updating everything here.
    // TODO: Timber in Fragments
    fun net(rootView: View) {
        val api = retrofit.create(MobileApi::class.java)

        api.report_card((activity.application as MyApplication).userState.studentID).enqueue(object : Callback<List<Classroom>> {
            override fun onResponse(call: Call<List<Classroom>>, response: Response<List<Classroom>>) {
                mSwipeRefreshLayout.isRefreshing = false
                if (response.isSuccessful) {
                    realm.executeTransaction {
                        realm.copyToRealmOrUpdate(response.body())
                    }
                    mAdapter.mData.addAll(response.body())
                } else {
                    Log.d("ClassesFragment", "Error: ${response.errorBody().string()}")
                    // FIXME: Finish
                }
            }

            override fun onFailure(call: Call<List<Classroom>>, t: Throwable) {
                mSwipeRefreshLayout.isRefreshing = false
                Log.e("ClassesFragment", "Error", t)
                Snackbar.make(rootView,
                        "No internet connection found! Please try again later.",
                        Snackbar.LENGTH_LONG).show()
            }
        })

    }

    fun initData() {
        val mClassrooms = realm.where(Classroom::class.java).findAll()
        mAdapter.mData.addAll(mClassrooms)
    }
}
