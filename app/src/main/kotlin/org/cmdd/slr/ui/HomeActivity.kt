package org.cmdd.slr.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.transition.TransitionInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import org.cmdd.slr.MyApplication
import org.cmdd.slr.R
import org.cmdd.slr.data.auth.AuthManager
import javax.inject.Inject

// TODO: Dynamic Toolbar color based on GPA
// FIXME: Save mApp.userState with savedInstanceState to avoid occasional crashes
class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar_home) lateinit var toolbar: Toolbar
    @BindView(R.id.drawer_layout) lateinit var drawer: DrawerLayout
    @BindView(R.id.nav_view) lateinit var navigationView: NavigationView

    @Inject lateinit var mAuth: AuthManager

    lateinit var schoolUrlString: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)

            window.exitTransition = TransitionInflater.from(this).inflateTransition(R.transition.home_activity_exit)
            window.allowEnterTransitionOverlap = true
            window.allowReturnTransitionOverlap = true
        }

        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)

        val component = (application as MyApplication).baseComponent
        component.inject(this)

        setSupportActionBar(toolbar)

        supportActionBar?.title = "Classes"
        val mApp = (application as MyApplication)
        schoolUrlString = mApp.userState.schoolUri.toString()

        // These methods are overriden to prevent the hamburger-to-back animation because it's not material
        val toggle = object : ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                super.onDrawerSlide(drawerView, 0f)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, 0f)
            }
        }
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        navigationView.setNavigationItemSelectedListener(this)

        val navHeader = navigationView.getHeaderView(0)
        val navHeaderName = navHeader.findViewById(R.id.nav_header_home_name) as TextView?
        val navHeaderSchool = navHeader.findViewById(R.id.nav_header_home_school) as TextView?

        navHeaderName?.text   = mApp.userState.fullName
        navHeaderSchool?.text = mApp.userState.schoolName

        onNavigationItemSelected(navigationView.menu.findItem(R.id.nav_home))
    }

    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout?
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            // TODO: Is this an anti-pattern?
            // TODO: Setting for this?
            AlertDialog.Builder(this)
                .setTitle(this.getString(R.string.home_dialog_leave_title))
                .setMessage(this.getString(R.string.home_dialog_leave_desc))
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", { dialog, which ->
                    finish()
                }).show()
            // super.onBackPressed()
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        navigationView.setCheckedItem(item.itemId)

        // TODO: Deal with tablets and such
        when (id) {
            R.id.nav_home -> {
                val tag = this.getString(R.string.home_tag_classes_fragment)
                val fm = supportFragmentManager
                val existingFragment = fm.findFragmentByTag(tag)
                if (existingFragment == null) {
                    val classesFragment = ClassesFragment()

                    val args = Bundle()
                    args.putString("base_url_string", schoolUrlString)
                    args.putString("jsid", null)
                    classesFragment.arguments = args
                    fm.beginTransaction()
                            .replace(R.id.fragment_container_home, classesFragment, tag).commit()
                }
            }

            R.id.nav_settings -> {
                val mIntent = Intent(this, SettingsActivity::class.java)
                startActivity(mIntent)
            }
            R.id.nav_log_out -> {
                mAuth.clearInfo()
                val mIntent = Intent(this, MainActivity::class.java)
                startActivity(mIntent)
                finish()
            }
        }

        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout?
        drawer!!.closeDrawer(GravityCompat.START)
        return true
    }
}
