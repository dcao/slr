package org.cmdd.slr.dagger.module

import dagger.Module
import dagger.Provides
import org.cmdd.slr.MyApplication
import javax.inject.Singleton

@Module
class AppModule(val mContext: MyApplication) {

    @Provides @Singleton
    fun provideMyApplication(): MyApplication {
        return mContext
    }
}