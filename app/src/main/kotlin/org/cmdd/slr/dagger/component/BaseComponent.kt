package org.cmdd.slr.dagger.component

import dagger.Component
import org.cmdd.slr.dagger.module.AppModule
import org.cmdd.slr.dagger.module.AuthModule
import org.cmdd.slr.data.auth.AuthManager
import org.cmdd.slr.ui.HomeActivity
import org.cmdd.slr.ui.MainActivity
import javax.inject.Singleton

@Singleton @Component(modules = arrayOf(
    AppModule::class, AuthModule::class
))
interface BaseComponent {
    // Only here because of ParseComponent
    fun authManager(): AuthManager

    fun inject(homeActivity: HomeActivity)
    fun inject(mainActivity: MainActivity)
}