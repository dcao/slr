package org.cmdd.slr.dagger.component

import dagger.Component
import org.cmdd.slr.dagger.module.NetModule
import org.cmdd.slr.dagger.scope.User
import retrofit2.Retrofit

@User @Component(modules = arrayOf(NetModule::class))
interface NetComponent {
    fun retrofit(): Retrofit
}