package org.cmdd.slr.dagger.module

import android.content.SharedPreferences
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import org.cmdd.slr.MyApplication
import org.cmdd.slr.data.auth.AesCbcWithIntegrity
import org.cmdd.slr.data.auth.AuthManager
import org.cmdd.slr.data.auth.SLKeyManager
import javax.inject.Singleton

@Module
class AuthModule(val mSecure: Boolean) {

    // TODO: SharedPreferences and {SLInfo <- AesCbcWithIntegrity} <- KeyManager interface

    @Provides @Singleton
    fun provideSharedPreferences(mContext: MyApplication): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
    }

    @Provides @Singleton
    fun provideAesCbcWithIntegrity(): AesCbcWithIntegrity {
        return AesCbcWithIntegrity()
    }

    @Provides @Singleton
    fun provideAuthManager(mPrefs: SharedPreferences): AuthManager {
        return SLKeyManager(mPrefs)
    }
}