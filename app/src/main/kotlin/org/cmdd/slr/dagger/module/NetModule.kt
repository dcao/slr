package org.cmdd.slr.dagger.module

import android.net.Uri
import android.util.Base64
import com.github.salomonbrys.kotson.*
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.realm.RealmList
import okhttp3.OkHttpClient
import org.apache.commons.lang3.math.NumberUtils
import org.cmdd.slr.dagger.scope.User
import org.cmdd.slr.data.SLInfo
import org.cmdd.slr.data.model.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import javax.inject.Named

@Module
class NetModule(val slInfo: SLInfo) {

    @Provides @Named("datetime") @User
    fun provideSimpleDateFormat(): SimpleDateFormat {
        return SimpleDateFormat("M/d/yy H:mm a")
    }

    @Provides @Named("datetimeUtc") @User
    fun provideUtcDateFormat(): SimpleDateFormat {
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    }

    @Provides @User
    fun provideGson(@Named("datetime") simpleDateFormat: SimpleDateFormat, @Named("datetimeUtc") utcDateFormat: SimpleDateFormat): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .registerTypeAdapter<UserState> {
                    deserialize {
                        val fullName = it.json["fullName"].string
                        val email = it.json["email"].string
                        val studentID = it.json["userID"].long
                        val schoolName = it.json["students"].asJsonArray[0]["school"]["name"].string
                        val schoolUri  = Uri.parse(it.json["students"].asJsonArray[0]["school"]["domainName"].string)
                        UserState(fullName, email, studentID, schoolName, schoolUri)
                    }
                }
                .registerTypeAdapter<Classroom> {
                    deserialize {
                        val classroom = Classroom()
                        classroom.courseName = it.json["courseName"].string
                        classroom.periodID = it.json["periodID"].long
                        classroom.period = it.json["period"].int
                        classroom.teacherID = it.json["teacherID"].long
                        classroom.teacherName = it.json["teacherName"].string
                        classroom.grade = it.json["grade"].string
                        val score = it.json["score"].string
                        classroom.score = score.substring(0, score.length - 1).toDouble()
                        classroom.lastUpdated = simpleDateFormat.parse(it.json["lastUpdated"].string)
                        classroom.coTeacherID = it.json["coTeacherID"].long
                        classroom.coTeacherName = it.json["coTeacherName"].string

                        classroom
                    }
                }
                .registerTypeAdapter<Grade> {
                    deserialize {
                        val grade = Grade()
                        grade.zero = it.json["zero"].bool
                        grade.scoreBase64 = it.json["scoreBase64"].string
                        grade.score = if (it.json["scoreBase64"].string == "null") null else it.json["score"].double * 100.00 / 100.00
                        grade.grade = if (it.json["grade"].string == "-") null else it.json["grade"].string
                        grade.changedDate = utcDateFormat.parse(it.json["changedDate"].string)
                        grade.excused = it.json["excused"].bool
                        grade.courseRank = it.json["courseRank"].int
                        grade.courseAverage = it.json["courseAverage"].double
                        grade.comment = it.json["comment"].string
                        grade.assignment = it.context.deserialize<Assignment>(it.json["assignment"])

                        grade
                    }
                }
                .registerTypeAdapter<Assignment> {
                    deserialize {
                        val assn = Assignment()
                        assn.dueDate = utcDateFormat.parse(it.json["dueDate"].string)
                        assn.title = it.json["title"].string
                        assn.maxPoints = it.json["maxPoints"].double
                        assn.systemID = it.json["systemID"].long
                        assn.categoryName = it.json["categoryName"].string

                        assn
                    }
                }
                .registerTypeAdapter<CourseDetail> {
                    deserialize {
                        val cd = CourseDetail()
                        val course = it.json[0]
                        val i = it

                        cd.periodID = course["periodID"].long
                        cd.lastUpdated = utcDateFormat.parse(course["date"].string)
                        cd.score = course["score"].double
                        cd.useWeighting = course["useWeighting"].bool
                        cd.grade = course["grade"].string
                        cd.periodAverage = course["grade"].nullString
                        cd.precision = course["precision"].int
                        cd.numberOfStudentsInCourse = course["numberOfStudentsInCourse"].int
                        cd.hasScore = course["hasScore"].bool
                        cd.dropped = course["dropped"].bool
                        cd.courseRank = course["courseRank"].int
                        cd.courseAverage = course["courseAverage"].double
                        cd.scoreChange = course["scoreChange"].double

                        val gradeDefs = course["gradeDefinitions"].array
                        cd.gradeDefinitions = RealmList<GradeDefinition>(*gradeDefs.map { i.context.deserialize<GradeDefinition>(it) }.toTypedArray())
                        cd.grades = RealmList<Grade>(*course["grades"].array.map {
                            if (it["scoreBase64"].string != "null" && it["assignment"].obj.has("maxPoints")) {
                                if (!NumberUtils.isCreatable(it["score"].string) && it["score"].string != "Does not count") {
                                    val elem = gradeDefs.find { def -> def["key"].string.replace(Regex("\\(.*?\\) ?"), "") == it["score"].string }
                                    if (elem != null) {
                                        if (elem["value"].string.endsWith("%")) {
                                            it["score"] = elem["value"].float * it["assignment"]["maxPoints"].float
                                        }
                                        it["score"] = elem["value"]
                                    }
                                }
                            } else {
                                it["scoreBase64"] = "null"
                            }
                            i.context.deserialize<Grade>(it)
                        }.toTypedArray())
                        cd.categories = RealmList<Category>(*course["categories"].array.toList().map { i.context.deserialize<Category>(it) }.toTypedArray())
                        cd.gradingScale = RealmList<Cutoff>(*course["GradingScale"]["Cutoffs"].array.toList().map { i.context.deserialize<Cutoff>(it) }.toTypedArray())

                        cd
                    }
                }
                .create()
    }

    @Provides @User
    fun provideOkHttpClient(): OkHttpClient {
        val creds = slInfo.username + ":" + slInfo.password
        val auth = "Basic " + Base64.encodeToString(creds.toByteArray(), Base64.NO_WRAP)

        val HEADER = "Mozilla/5.0 (Linux; Android 7.0; Nexus 5 Build/NBD90Z; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/54.0.2840.68 Mobile Safari/537.36"

        return OkHttpClient.Builder()
                .addInterceptor {
                    val orig = it.request()

                    val request = orig.newBuilder()
                            .header("Authorization", auth)
                            .header("Accept", "application/json")
                            .header("X-Requested-With", "com.schoolloop.mobileloop.app")
                            .header("User-Agent", HEADER)
                            .method(orig.method(), orig.body())
                            .build()
                    it.proceed(request)
                }.build()
    }

    // TODO: Figure out auto-retry for IOExceptions
    @Provides @User
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        val url = slInfo.baseUri.buildUpon()
            .scheme("https")
            .build().toString()

        return Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    // TODO: Just provide an instance of MobileApi directly?
}
