package org.cmdd.slr.dagger.scope

import javax.inject.Scope

@Scope
annotation class User
